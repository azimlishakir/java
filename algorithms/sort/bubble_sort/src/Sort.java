public class Sort {

    public static int [] bubblesort(int [] bubble){
        for (int i=0; i<bubble.length-1;i++){
            for (int j=0;j<bubble.length-1-i;j++){
                if (bubble[j] > bubble[j+1]){
                    int temp = bubble[j];
                    bubble[j]=bubble[j+1];
                    bubble[j+1] = temp;
                }
            }
        }


        return bubble;
    }
    public static int [] selectionsort(int [] select){
        for (int i=0;i<select.length-1;i++){
            for (int j=i;j<select.length;j++){
                if (select[i] > select[j]){
                    int tmp=select[i];
                    select[i]=select[j];
                    select[j]=tmp;
                }
            }
        }

        return select;


    }

    public static int [] insertion(int [] insert){

      for (int i=1;i<insert.length;i++){
          int key = insert[i];
          int j= i-1;

          while ((j > -1) && (insert[j] > key)){
              insert[j+1]=insert[j];
              j--;
          }

          insert[j+1]=key;

       }

        return insert;
    }




}
