import java.util.*;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        List<String> alphabet = new ArrayList<>();
        alphabet.add("A");
        alphabet.add("B");
        alphabet.add("C");
        alphabet.add("D");


        Collections.reverse(alphabet);

        for (String s : alphabet){
            System.out.println(s);
        }
    }
}
