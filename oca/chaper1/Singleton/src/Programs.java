public class Programs {
    static int count;

    {
        System.out.println("Object yaradildi! " + count++);
    }

    public Programs() {

    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Qaldi: "+ --count);
    }
}
